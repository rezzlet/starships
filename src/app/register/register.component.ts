﻿import { Component } from '@angular/core';
import { User } from '../_interfaces/user.interface';
import { UserService } from '../_services/user.service.local-storage';
import { FlashService } from '../_services/flash.service';
import { Router } from '@angular/router';

@Component({templateUrl: './register.component.html'})
export class RegisterComponent {

    public user: User = {} as User;
    public dataLoading = false;

    constructor(private userService: UserService,
        private flashService: FlashService,
        private router: Router) {}

    public register() {
        this.dataLoading = true;
        this.userService.create(this.user)
            .subscribe((response) => {
                if (response.success) {
                    this.flashService.success('Registration successful', true);
                    this.router.navigate(['login']);
                } else {
                    this.flashService.error(response.message);
                    this.dataLoading = false;
                }
            });
    }

}
