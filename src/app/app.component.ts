import { Component, OnInit, OnDestroy } from '@angular/core';
import { FlashService, FlashMessage } from './_services/flash.service';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthenticationService } from './_services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public flash: FlashMessage;

  private subs: Subscription[] = [];

  constructor(private flashService: FlashService,
    private router: Router,
    private cookieService: CookieService,
    private authenticationService: AuthenticationService) {}

  public ngOnInit() {
    const flashSub = this.flashService.emitter$.subscribe( (flash) => {
      this.flash = flash;
    });
    const routerSub = this.router.events.subscribe( ( event ) => {
      if (event instanceof NavigationEnd) {
        if (this.flash && !this.flash.keepAfterLocationChange) {
          this.flash = null;
        }
      }
    });
    this.subs.push(flashSub, routerSub);
    this.checkLoggedIn();
  }

  public ngOnDestroy() {
    this.subs.forEach( (sub) => {
      sub.unsubscribe();
    });
  }

  private checkLoggedIn() {
    if (this.cookieService.get('globals')) {
      this.authenticationService.currentUser$.next(JSON.parse(this.cookieService.get('globals')));
    }
  }
}
