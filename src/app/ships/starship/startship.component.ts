import { Component, Input, OnInit } from '@angular/core';
import { Ship } from 'src/app/_interfaces/ship.interface';

@Component({
    selector: 'app-starship',
    templateUrl: './starship.component.html',
    styleUrls: ['./starship.component.scss']
})
export class StarshipComponent implements OnInit {
    @Input()
    public data: Ship;

    public shipId: string;

    ngOnInit() {
        const url = this.data.url;
        const chunks = url.split('/').filter(function (item) {
            return item !== '';
        }).slice(-1);
        this.shipId  = chunks[0];
    }
}
