import { Component, OnInit } from '@angular/core';
import { ShipsService } from '../_services/ships.service';
import { Ship } from '../_interfaces/ship.interface';
import { ShipResponse } from '../_interfaces/ship-response.interface';

@Component({templateUrl: './ships.component.html'})
export class ShipsComponent implements OnInit {

    private lastResponse: ShipResponse;

    public starships: Ship[] = [];
    public error = false;

    constructor(private shipService: ShipsService) {}

    public ngOnInit() {
        this.fetchNextPage();
    }

    public fetchNextPage() {
        const url = this.lastResponse ? this.lastResponse.next : null;
        this.shipService.getStarships(url).subscribe( (data) => {
            this.starships = this.starships.concat(data.results);
            this.lastResponse = data;
        }, () => {
            this.error = true;
        })
    }
}