import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShipResponse } from '../_interfaces/ship-response.interface';

@Injectable()
export class ShipsService {

    constructor(private http: HttpClient) {}
    getStarships(url) {
        url  = url || 'https://swapi.co/api/starships/';
        return this.http.get<ShipResponse>(url);
    }
}
