import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { User } from '../_interfaces/user.interface';
import { LoginResponse } from '../_interfaces/login-response.interface';

@Injectable()
export class UserService {

    public getAll() {
        return this.getUsers();
    }

    public getById(id: number) {
        const filtered = this.getUsers().filter( (current) => {
            return current.id === id;
        });
        return filtered.length > 0 ? filtered[0] : null;
    }

    public getByUsername(username: string) {
        const filtered = this.getUsers().filter( (current) => {
            return current.username === username;
        });
        return filtered.length > 0 ? filtered[0] : null;
    }

    public create(user: User) {
        const duplicateUser = this.getByUsername(user.username);

        let rs: Observable<LoginResponse>;

        if (duplicateUser !== null) {
            rs = of({ success: false, message: 'Username "' + user.username + '" is already taken' });
        } else {
            const users = this.getUsers();

            // assign id
            const lastUser = users[users.length - 1] || { id: 0 };
            user.id = lastUser.id + 1;

            // save to local storage
            users.push(user);
            this.setUsers(users);

            rs = of({ success: true }).pipe(delay(1000));
        }

        return rs;
    }

    public update(user: User) {
        const users = this.getUsers();
        for (let i = 0; i < users.length; i++) {
            if (users[i].id === user.id) {
                users[i] = user;
                break;
            }
        }
    }
    public delete(id: number) {
        const users = this.getUsers();
        for (let i = 0; i < users.length; i++) {
            const user = users[i];
            if (user.id === id) {
                users.splice(i, 1);
                break;
            }
        }
        this.setUsers(users);
    }
    private getUsers(): User[] {
        if (!localStorage.users) {
            localStorage.users = JSON.stringify([]);
        }

        return JSON.parse(localStorage.users);
    }
    private setUsers(users: User[]) {
        localStorage.users = JSON.stringify(users);
    }
}
