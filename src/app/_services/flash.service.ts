import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export interface FlashMessage {
    message: string;
    type: FlashMessageType;
    keepAfterLocationChange: boolean;
}
export enum FlashMessageType {
    ERROR = 'error',
    SUCESSS = 'success'
}
@Injectable()
export class FlashService {
    public emitter$: Subject<FlashMessage> = new Subject();
    public success(message: string, keepAfterLocationChange?: boolean) {
        this.emitter$.next({message: message, type: FlashMessageType.SUCESSS, keepAfterLocationChange: keepAfterLocationChange})
    }
    public error(message: string, keepAfterLocationChange?: boolean) {
        this.emitter$.next({message: message, type: FlashMessageType.ERROR, keepAfterLocationChange: keepAfterLocationChange})
    }
}
