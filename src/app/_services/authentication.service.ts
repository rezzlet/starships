import { Injectable } from '@angular/core';
import { UserService } from './user.service.local-storage';
import { of, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs/operators';
import { CurrentUser } from '../_interfaces/current-user.interface';
import { User } from '../_interfaces/user.interface';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthenticationService {
    public currentUser$: BehaviorSubject<CurrentUser> = new BehaviorSubject(null);

    constructor(private userService: UserService,
        private http: HttpClient,
        private cookieService: CookieService) {}

    public login(username, password) {

        /* Dummy authentication for testing, uses $timeout to simulate api call
        ----------------------------------------------*/
        const user: User = this.userService.getByUsername(username);
        let response;
        if (user !== null && user.password === password) {
            response = of({ success: true });
        } else {
            response = of({ success: false, message: 'Username or password is incorrect' });
        }

        return response.pipe(delay(1000));

        /* Use this for real authentication
        ----------------------------------------------*/
        return this.http.post('/api/authenticate', { username: username, password: password });

    }

    public setCredentials(username, password) {
        const authdata = btoa(`${username}:${password}`);

        this.currentUser$.next({username: username, authdata: authdata });

        // store user details in globals cookie that keeps user logged in for 1 week (or until they logout)
        const cookieExp = new Date();
        cookieExp.setDate(cookieExp.getDate() + 7);
        this.cookieService.set('globals', JSON.stringify(this.currentUser$.getValue()), cookieExp);
    }

    public clearCredentials() {
        this.currentUser$.next(null);
        this.cookieService.delete('globals');
    }

}
