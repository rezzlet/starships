import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class RestrictedGuardService implements CanActivate {
    constructor(private cookieService: CookieService,
        private router: Router) {}
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        const isLoggedIn = this.cookieService.get('globals');

        if (!isLoggedIn) {
            this.router.navigate(['/login']);
            return false;
        }

        return true;
    }
}
