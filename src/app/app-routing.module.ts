import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShipsComponent } from './ships/ships.component';
import { RestrictedGuardService } from './_guards/restricted.guard';

const routes: Routes = [
  {
    path: '', pathMatch: 'full', redirectTo: 'ships'
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'register', component: RegisterComponent
  },
  {
    path: 'ships', component: ShipsComponent, canActivate: [
      RestrictedGuardService]
  },
  {
    path: '**', redirectTo: ''
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
