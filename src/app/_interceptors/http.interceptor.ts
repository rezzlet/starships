import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../_services/authentication.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(public authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.startsWith('https://swapi.co/api/starships/')) {
      request = request.clone({
        setHeaders: {
          Authorization: this.getAuthHeader()
        }
      });
    }

    return next.handle(request);
  }

  private getAuthHeader() {
    const isLoggedIn = this.authenticationService.currentUser$.getValue() !== null;
    const baseHeader = 'Basic';
    return isLoggedIn ?
      baseHeader + ' ${this.authenticationService.currentUser$.getValue().authdata}' :
      baseHeader;
  }
}
