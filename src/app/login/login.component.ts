import { Component } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { FlashService } from '../_services/flash.service';
import { Router } from '@angular/router';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent {

    public username: string;
    public password: string;

    public dataLoading = false;

    constructor(private authenticationService: AuthenticationService,
        private flashService: FlashService,
        private router: Router) {}

    public login() {
        this.dataLoading = true;
        this.authenticationService.login(this.username, this.password).subscribe((response) => {
            if (response.success) {
                this.authenticationService.setCredentials(this.username, this.password);
                this.router.navigate(['ships']);
            } else {
                this.flashService.error(response.message);
                this.dataLoading = false;
            }
        });
    };
}
