import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CookieService } from 'ngx-cookie-service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FlashService } from './_services/flash.service';
import { UserService } from './_services/user.service.local-storage';
import { ShipsService } from './_services/ships.service';
import { AuthenticationService } from './_services/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { ShipsComponent } from './ships/ships.component';
import { StarshipComponent } from './ships/starship/startship.component';
import { RestrictedGuardService } from './_guards/restricted.guard';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ShipsComponent,
    StarshipComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    CookieService,
    FlashService,
    UserService,
    ShipsService,
    AuthenticationService,
    RestrictedGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
